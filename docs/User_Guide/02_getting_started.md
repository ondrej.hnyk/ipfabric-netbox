---
description: Overview of how to get started with the IP Fabric NetBox Plugin.
---

# Getting Started

## Install the Plugin

To install the plugin, please follow the [installation instructions](../administration/01_install.md).
