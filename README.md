# Overview

This plugin allows the integration and data synchronization between IP Fabric and NetBox.

The plugin uses IP Fabric collect network data utilizing the [IP Fabric Python SDK](https://gitlab.com/ip-fabric/integrations/python-ipfabric). This plugin relies on helpful features in NetBox like [Staged Changes](https://docs.netbox.dev/en/stable/plugins/development/staged-changes/) and [Background Tasks](https://docs.netbox.dev/en/stable/plugins/development/background-tasks) to make the job of bringing in data to NetBox easier.

- Multiple IP Fabric Sources
- Transform Maps
- Scheduled Synchronization
- Diff Visualization

## NetBox Compatibility

| Netbox Version | Plugin Version |
| -------------- | -------------- |
| 3.4            | <=1.0.11       |
| 3.5            | <=1.0.11       |
| 3.6            | <=1.0.11       |
| 3.7            | >=2.0.0        |


## Screenshots

Sources.

Snapshots.

Transform Maps.

Ingestion.

Diff Visualization.

## Documentation

Full documentation for this plugin can be found at [IP Fabric Docs](https://docs.ipfabric.io/main/integrations/netbox/).

- User Guide
- Administrator Guide
